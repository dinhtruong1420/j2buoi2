function tinhTien() {
	var luongMotNgay = document.getElementById("luongmotngay").value;
	var soNgayLam = document.getElementById("songaylam").value;
	var luong = soNgayLam * luongMotNgay;
	document.getElementById("result").innerHTML = `<p>Lương của bạn:${luong}</p>`;
	console.log("result:", luong);
}

function tinhTrungBinh() {
	var soThuNhat = document.getElementById("sothunhat").value;
	var soThuHai = document.getElementById("sothuhai").value;
	var soThuBa = document.getElementById("sothuba").value;
	var soThuTu = document.getElementById("sothutu").value;
	var soThuNam = document.getElementById("sothunam").value;
	var trungBinhTong = (soThuNhat * 1 + soThuHai * 1 + soThuBa * 1 + soThuTu * 1 + soThuNam * 1) / 5;
	document.getElementById("result1").innerHTML = `<p>Trung Bình Tổng là:${trungBinhTong}</p>`
}

function quyDoiTien() {
	var tienDo = document.getElementById("sotienusd").value;
	var tienQuyDoi = new Intl.NumberFormat('vn-VN').format(tienDo * 23500);
	document.getElementById('result2').innerHTML = `<p>Tiền quy đổi là:${tienQuyDoi}</p>`
}

function tinh() {
	var chieuDai = document.getElementById('chieudai').value;
	var chieuRong = document.getElementById('chieurong').value;
	var chuVi = (chieuDai * 1 + chieuRong * 1) * 2;
	var dienTich = (chieuDai * chieuRong);
	document.getElementById('result3').innerHTML = `<p>
	Diện tích:${dienTich}</p>
	<p>Chu vi:${chuVi}</p>`;
}

function tinhTong() {
	var soTuNhienCoHaiChuSo = document.getElementById('socohaichuso').value;
	var soHangChuc = Math.floor(soTuNhienCoHaiChuSo / 10);
	var soHangDonVi = soTuNhienCoHaiChuSo % 10;
	var tongHaiKiSo = soHangChuc * 1 + soHangDonVi * 1;
	document.getElementById('result4').innerHTML = `<p>Tổng hai kí số:${tongHaiKiSo}</p>`
}